You _**will no longer**_ be able to:

+ add or delete problems
+ modify the position of the student ID widget
+ modify the position of the page markers
