import React from 'react'

const DropzoneContent = () => (
  <div className='file has-name is-boxed is-centered'>
    <label className='file-label'>
      <span className='file-cta'>
        <span className='file-icon'>
          <i className='fa fa-upload' />
        </span>
        <span className='file-label'>
                    Choose a file…
        </span>
      </span>
    </label>
  </div>
)

export default DropzoneContent
